extends Control


signal btn_info_back_pressed


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	emit_signal("btn_info_back_pressed")
