extends Control


# BUILTINS - - - - - - - - -


# енум имен сцен
enum VIEWS {
	GUI,
	LEVELS,
	GAME,
	MENU,
	BONUSES,
	INFO_1,
	INFO_2,
}
# имена всех сцен
const SCENES_MAP: PoolStringArray = PoolStringArray([
	"GUI",
	"Levels",
	"Game",
	"Menu",
	"Bonuses",
	"Info1",
	"Info2",
])
# список активных сцен
export(VIEWS) var ENTRY_SCENE = VIEWS.LEVELS
export(bool) var SHOW_GUI_AT_START = true
export(Array, VIEWS) var ACTIVE_SCENES
var active_view: int = ENTRY_SCENE


const TWEEN_SPEED: float = 0.3


func _ready() -> void:
	randomize()

	# подключение сигналов
	var _c: int = get_node(SCENES_MAP[VIEWS.LEVELS]).connect("level_pressed", self, "_on_Levels_level_pressed")

	_c = get_node(SCENES_MAP[VIEWS.GUI]).connect("btn_back_pressed", self, "_on_GUI_btn_back_pressed")
	_c = get_node(SCENES_MAP[VIEWS.GUI]).connect("btn_spin_pressed", self, "_on_GUI_btn_spin_pressed")
	_c = get_node(SCENES_MAP[VIEWS.GUI]).connect("btn_autospin_pressed", self, "_on_GUI_btn_autospin_pressed")

	_c = get_node(SCENES_MAP[VIEWS.GAME]).connect("score_increased", self, "_on_Game_score_increased")
	_c = get_node(SCENES_MAP[VIEWS.GAME]).connect("spinnig_changed", self, "_on_Game_spinnig_changed")
	_c = get_node(SCENES_MAP[VIEWS.GAME]).connect("auto_spinnig_changed", self, "_on_Game_auto_spinnig_changed")
	_c = get_node(SCENES_MAP[VIEWS.GAME]).connect("autospin_count_changed", self, "_on_Game_autospin_count_changed")

	_c = get_node(SCENES_MAP[VIEWS.MENU]).connect("btn_slots_pressed", self, "_on_Menu_btn_slots_pressed")
	_c = get_node(SCENES_MAP[VIEWS.MENU]).connect("btn_bonuses_pressed", self, "_on_Menu_btn_bonuses_pressed")
	_c = get_node(SCENES_MAP[VIEWS.MENU]).connect("btn_info_pressed", self, "_on_Menu_btn_info_pressed")

	_c = get_node(SCENES_MAP[VIEWS.INFO_1]).connect("btn_info_back_pressed", self, "_on_Info_btn_info_back_pressed")
	_c = get_node(SCENES_MAP[VIEWS.INFO_2]).connect("btn_info_back_pressed", self, "_on_Info_btn_info_back_pressed")

	_c = get_node(SCENES_MAP[VIEWS.BONUSES]).connect("btn_bonus_pressed", self, "_on_Bonuses_btn_bonus_pressed")

	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_controls", false)

	set_view(ENTRY_SCENE, SHOW_GUI_AT_START)
	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", false)


# METHODS - - - - - - - - -


# менеджер сцен
func set_view(view: int, show_gui: bool) -> void:
	active_view = view
	var _t: bool
	(get_node(SCENES_MAP[view]) as Control).show()
	for i in ACTIVE_SCENES:
		if i != view and i != VIEWS.GUI or i == VIEWS.GUI and not show_gui:
			_t = ($Tween as Tween).interpolate_property(get_node(SCENES_MAP[i]), "modulate:a", null, 0.0, TWEEN_SPEED)
		else:
			_t = ($Tween as Tween).interpolate_property(get_node(SCENES_MAP[i]), "modulate:a", null, 1.0, TWEEN_SPEED / 2.0)
			(get_node(SCENES_MAP[i]) as Control).show()
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(TWEEN_SPEED), "timeout")
	for i in ACTIVE_SCENES:
		if i != view and i != VIEWS.GUI or i == VIEWS.GUI and not show_gui:
			(get_node(SCENES_MAP[i]) as Control).hide()


# SIGNALS - - - - - - - - -


# при выборе уровня
func _on_Levels_level_pressed(level: int) -> void:
	set_view(VIEWS.GAME, true)
	get_node(SCENES_MAP[VIEWS.GAME]).call("prepare_level", level)
	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_controls", true)
	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", true)


# при нажатии кнопки назад в шапке
func _on_GUI_btn_back_pressed() -> void:
	match active_view:
		VIEWS.GAME:
			set_view(VIEWS.LEVELS, true)
			get_node(SCENES_MAP[VIEWS.GAME]).call("clear_level")
			get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_controls", false)
			get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", false)
		VIEWS.LEVELS, VIEWS.BONUSES:
			set_view(VIEWS.MENU, true)
			get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", false)


# при нажатии на кнопку слотов в меню
func _on_Menu_btn_slots_pressed() -> void:
	set_view(VIEWS.LEVELS, true)
	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", true)


func _on_Menu_btn_bonuses_pressed() -> void:
	set_view(VIEWS.BONUSES, true)
	get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_btn_back", true)


# при нажатии на кнопку информации в меню
func _on_Menu_btn_info_pressed(page: int) -> void:
	if page == 1:
		set_view(VIEWS.INFO_1, false)
	elif page == 2:
		set_view(VIEWS.INFO_2, false)


# при нажатии на кнопку назад на экране информации
func _on_Info_btn_info_back_pressed() -> void:
	set_view(VIEWS.MENU, true)


# при нажатии на кнопку бонуса на экране бонусов
func _on_Bonuses_btn_bonus_pressed(bonus: int) -> void:
	get_node(SCENES_MAP[VIEWS.GUI]).call("set_bonus_score", bonus)


# при нажатии на кнопку спина
func _on_GUI_btn_spin_pressed() -> void:
	get_node(SCENES_MAP[VIEWS.GAME]).call("spin")


# при нажатии на кнопку автоспина
func _on_GUI_btn_autospin_pressed() -> void:
	get_node(SCENES_MAP[VIEWS.GAME]).call("auto_spin")


# при изменении очков
func _on_Game_score_increased() -> void:
	get_node(SCENES_MAP[VIEWS.GUI]).call("set_score")


# при изменении статуса спина
func _on_Game_spinnig_changed(status: bool) -> void:
	get_node(SCENES_MAP[VIEWS.GUI]).set("spinning", status)


# при изменении статуса автоспина
func _on_Game_auto_spinnig_changed(status: bool) -> void:
	get_node(SCENES_MAP[VIEWS.GUI]).set("auto_spinning", status)
	if not status:
		get_node(SCENES_MAP[VIEWS.GUI]).call("show_hide_autospin_progress", false)


# при изменении количества повторов во время автоспина
func _on_Game_autospin_count_changed() -> void:
	get_node(SCENES_MAP[VIEWS.GUI]).call("change_autospin_count")

