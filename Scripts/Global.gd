extends Node


# енум имен сцен
enum VIEWS {
	GUI,
	LEVELS,
	GAME,
	MENU,
	INFO,
}


# GUI
const GIFT_PRICE: int = 50000 # gift price
const FOOTER_EXTRA_OFFSET: float = 200.0 # extra offset for hiding footer in "show_hide_controls()"


# GAME
const SPIN_TIME: float = 2.3 # время кручения после разгона
# const STOPPING_SPIN_TIME: float = 1.5 # время остановки кручения
var AUTO_SPIN_COUNT: int = 1 # счетчик количества повторов автоспина
const AUTO_SPIN_MAX: float = 10.0 # количество повторов автоспина

const BGS: Array = [
	preload("res://Resources/Sprites/bg_lvl_1-min.png"),
	preload("res://Resources/Sprites/bg_lvl_2-min.png"),
	preload("res://Resources/Sprites/bg_lvl_3-min.png"),
]
const BOARDS: Array = [
	preload("res://Resources/Sprites/board_lvl_1-min.png"),
	preload("res://Resources/Sprites/board_lvl_2-min.png"),
	preload("res://Resources/Sprites/board_lvl_3-min.png"),
]
const SHADOWS: Array = [
	[
#		preload("res://source/assets/image/shadow_top_lvl_1.png"),
#		preload("res://source/assets/image/shadow_bottom_lvl_1.png"),
		null, null,
	],
	[
		null, null,
	],
	[
		null, null,
	],
	[
		null, null,
	],
]

const PROPS: = [
	{
		w = 300,
		h = 300,
		count_x = 3, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = 20, # отступ между иконками
		offset_y = 22,
		variations = 4, # вариации иконок
		icons_offset_x = 0, # отступ иконок
		icons_offset_y = -63,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = -55,
		shadow_top_offset_y = 0, # отступ верхней тени
		shadow_bottom_offset_y = 0, # отступ нижней тени
	},
	{
		w = 300,
		h = 300,
		count_x = 3,
		count_y = 3,
		offset_x = 27,
		offset_y = 30,
		variations = 5,
		icons_offset_x = 5,
		icons_offset_y = -65,
		board_bg_offset_x = 0,
		board_bg_offset_y = -55,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
	{
		w = 300,
		h = 300,
		count_x = 4,
		count_y = 4,
		offset_x = -50,
		offset_y = -74,
		variations = 5,
		icons_offset_x = 0,
		icons_offset_y = -55,
		board_bg_offset_x = 0,
		board_bg_offset_y = -55,
		shadow_top_offset_y = 0,
		shadow_bottom_offset_y = 0,
	},
]


func set_autospin_count(count: int) -> void:
	AUTO_SPIN_COUNT = count
